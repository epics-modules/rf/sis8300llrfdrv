#include <stdint.h>

#include <sis8300drv.h>
#include <sis8300drv_utils.h>

#include "sis8300llrfdrv.h"
#include "sis8300llrf_reg.h"
#include "sis8300llrfdrv_types.h"

static inline int _sis8300llrfdrv_get_rw_ctrl_table_params(
                    sis8300drv_usr *sisuser, 
                    sis8300llrfdrv_ctrl_table ctrl_table, 
                    uint32_t *base_table_offset, 
                    unsigned *max_nsamples);

static const uint32_t table_mode_addr[2] = {
    SIS8300LLRF_FF_TABLE_CYCLIC_EN_REG,
    SIS8300LLRF_SP_TABLE_CYCLIC_EN_REG
    };

static const uint32_t table_mode_shift[2] = {
    8,
    8,
    };

/**
 * @brief   Set Table mode, circular or hold last
 *
 * @param [in] sisuser  User context Struct
 * @param [in]  ctrl_table  Either ff or sp, #sis8300llrfdrv_ctrl_table
 * @param [in] mode     Desired mode, #sis8300llrfdrv_table_mode
 *
 * @return status_success       Success
 * @return status_device_armed  Operatin is not allowed when the device
 *                              is armed
 * @return status_no_device     Device not opened
 * @return status_device_access Cannot access device registers
 *
 * Circular mode is only meant for special operating modes, so take care!
 *
 * Calls to this function are serialized with respect to other calls 
 * that alter the functionality of the device. This means that this 
 * function may block.
 */
int sis8300llrfdrv_set_table_mode(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_ctrl_table ctrl_table,
        sis8300llrfdrv_table_mode mode) {
    
    int status;
    sis8300drv_dev *sisdevice;
    uint32_t ui32_reg_val;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    //TODO: double check this, because special mode doesn't exist anymore
/*
    In special operating modes this is allowed even if 
    the device is armed
    
    if (sisdevice->armed) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_armed;
    }
*/
    status = sis8300_reg_read(sisdevice->handle, 
                table_mode_addr[ctrl_table], &ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    switch (mode) {
        case table_mode_normal:
            ui32_reg_val &= ~(0x1 << table_mode_shift[ctrl_table]);
            break;
        case table_mode_circular:
            ui32_reg_val |= 0x1 << table_mode_shift[ctrl_table];
            break;
        default:
            pthread_mutex_unlock(&sisdevice->lock);
            return status_argument_invalid;
    }

    status = sis8300_reg_write(sisdevice->handle, 
                table_mode_addr[ctrl_table], ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get table mode
 *
 * @param [in]  sisuser  User context struct
 * @param [in]  ctrl_table  Either ff or sp, #sis8300llrfdrv_ctrl_table
 * @param [out] mode     will hold ff table mode on success
 *
 * @return status_success          Information retrieved successfully
 * @return status_device_access    Can't access device registers
 * @return status_no_device        Device not opened
 *
 * Returns the trigger setup that is currently used by the device.
 */
int sis8300llrfdrv_get_table_mode(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_ctrl_table ctrl_table,
        sis8300llrfdrv_table_mode *mode) {
    
    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                table_mode_addr[ctrl_table], &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    *mode = (ui32_reg_val & (0x1 << table_mode_shift[ctrl_table]) ) ? 
                table_mode_circular : table_mode_normal;

    return status_success;
}


/**
 * @brief Get the maximum allowed size of FF or SP table in 256 bit 
 *        chunks
 *
 * @param [in]  sisuser     User context struct
 * @param [in]  ctrl_table  Either ff or sp, #sis8300llrfdrv_ctrl_table
 * @param [out] table_size  Allowed table size -> number of 256 blocks
 *
 * @return status_success           Data retrieved successfully
 */
int sis8300llrfdrv_get_ctrl_table_max_nelm(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        uint32_t *max_nelm) {
    *max_nelm = SIS8300LLRFDRV_CTRL_TABLE_NELM_MAX-1;
    return status_success;
}

/**
 * @brief Set number of samples for ff or sp table that is in use
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  ctrl_table Either ff or sp, #sis8300llrfdrv_ctrl_table
 * @param [out] table_size Allowed table size -> number of 256 blocks
 *
 * @return status_success           Data retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_argument_range    nsamples value is out of range
 * @return status_no_device         Device not opened
 * @return status_argument_invalid  Wrong ctrl_table choice
 *
 * nsamples can be between:
 *      0 and 2^12 - 1 (12 bits unsigned) for SP table
 *      0 and 2^15 - 1 (15 bits unsigned) for FF table
 *
 * Calls to this function are serialized with respect to other calls 
 * that alter the functionality of the device. This means that this 
 * function may block.
 */
int sis8300llrfdrv_set_ctrl_table_nelm(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        uint32_t nelm) {

    int status;
    uint32_t ui32_reg_val = 0, base_offset = 0;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    /* get reg addr */
    switch (ctrl_table) {
    case ctrl_table_sp:
        base_offset = SIS8300LLRF_LUT_CTRL_SP_NSAMPLES_REG;
        break;
    case ctrl_table_ff:
        base_offset = SIS8300LLRF_LUT_CTRL_FF_NSAMPLES_REG;
        break;
    default:
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_invalid;
    }

    /* check limits */

    uint32_t max_elem;
    status = sis8300llrfdrv_get_ctrl_table_max_nelm(sisuser, ctrl_table, &max_elem);
    if (nelm > max_elem) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_range;
    }

    /* read the current val to preserve ff table mode setting */
    status = sis8300_reg_read(sisdevice->handle, 
                base_offset, &ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    /* set the new value */
    ui32_reg_val &= ~SIS8300LLRF_LUT_CTRL_NSAMPLES_MASK;
    ui32_reg_val |= nelm & SIS8300LLRF_LUT_CTRL_NSAMPLES_MASK;

    /* write val */
    status = sis8300_reg_write(sisdevice->handle, 
                base_offset, ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get number of samples for ff or sp table that is in use
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  ctrl_table Either ff or sp, #sis8300llrfdrv_ctrl_table
 * @param [out] table_size Allowed table size -> number of 256 blocks
 *
 * @return status_success           Data retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 * @return status_argument_invalid  Wrong ctrl_table choice
 *
 * Samples are IQ points, 32 bit words consisting of 16 bit angle and 16
 * bit magnitude values
 */
int sis8300llrfdrv_get_ctrl_table_nelm(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        uint32_t *nelm) {

    int status;
    uint32_t ui32_reg_val = 0, base_offset = 0;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    /* get reg addr */
    switch (ctrl_table) {
    case ctrl_table_sp:
        base_offset = SIS8300LLRF_LUT_CTRL_SP_NSAMPLES_REG;
        break;
    case ctrl_table_ff:
        base_offset = SIS8300LLRF_LUT_CTRL_FF_NSAMPLES_REG;
        break;
    default:
        return status_argument_invalid;
    }

    /* read register */
    status = sis8300_reg_read(sisdevice->handle, 
                base_offset, &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    /* set value */
    *nelm = (uint32_t) (ui32_reg_val & SIS8300LLRF_LUT_CTRL_NSAMPLES_MASK);

    return status_success;
}


static const uint32_t table_speed_addr[2] = {
    SIS8300LLRF_FF_TABLE_SPEED_REG,
    SIS8300LLRF_SP_TABLE_SPEED_REG
};

static const uint32_t table_speed_mask[2] = {
    0x7F,
    0x7F,
    };


/**
 * @brief Set the table speed - setting is the same for I and Q 
 *        controller
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  ctrl_table   sp or ff 
 * @param [in]  speed      Can be betwen 0 and 255(0xFF),
 *                         0x0: New table value with every sample (sample-synchronous)
 *                         1- 255 (every 1 to 255 clock cycle) - clock synchronous
 *
 * @return status_success           Data written successfully
 * @return status_device_access     Can't access device registers
 * @return status_argument_range    nsamples value is out of range
 * @return status_no_device         Device not opened
 *
 * The function sets the rate at which next SP value is added to the 
 * PI-output. The setting is the same for both I and Q controller.
 *
 * Calls to this function are serialized with respect to other calls that 
 * alter the functionality of the device. This means that this function 
 * may block.
 */
int sis8300llrfdrv_set_ctrl_table_speed(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned speed) {

    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    if (speed > 0xFF) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_range;
    }

    if ((ctrl_table != ctrl_table_sp) && (ctrl_table != ctrl_table_ff))
        return status_argument_invalid;

    status = sis8300_reg_read(
                sisdevice->handle, 
                table_speed_addr[ctrl_table], 
                &ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    ui32_reg_val &= ~table_speed_mask[ctrl_table];
    ui32_reg_val |= ((uint32_t) speed);

    status = sis8300_reg_write(sisdevice->handle, 
                table_speed_addr[ctrl_table], ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get the table speed - setting is the same for I and Q 
 *        controller
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  ctrl_table   sp or ff 
 * @param [in]  speed      Can be betwen 0 and 255(0xFF),
 *                         0x0: New table value with every sample (sample-synchronous)
 *                         1- 255 (every 1 to 255 clock cycle) - clock synchronous
  *
 * @return status_success           Data retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 *
 * The function sets the rate at which next FF value is added to the 
 * PI-output. The setting is the same for both I and Q controller.
 */
int sis8300llrfdrv_get_ctrl_table_speed(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table,
        unsigned *speed) {

    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    if ((ctrl_table != ctrl_table_sp) && (ctrl_table != ctrl_table_ff))
        return status_argument_invalid;

    status = sis8300_reg_read(sisdevice->handle, 
                table_speed_addr[ctrl_table], &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    ui32_reg_val &= table_speed_mask[ctrl_table];
    *speed = (unsigned) (ui32_reg_val);

    return status_success;
}

//Fixed value enable
//   SIS8300LLRF_SP_TABLE_CONSTANT_EN_REG, SIS8300LLRF_FF_TABLE_CONSTANT_EN_REG,


static const uint32_t table_cnst_en_addr[2] = {
    SIS8300LLRF_FF_TABLE_CONSTANT_EN_REG,
    SIS8300LLRF_SP_TABLE_CONSTANT_EN_REG 
};


/**
 * @brief Set the controller to use constant value or not - 
 * setting is the same for I and Q 
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  ctrl_table   sp or ff 
 * @param [in]  enable      0 or 1 
 *
 * @return status_success           Data written successfully
 * @return status_device_access     Can't access device registers
 * @return status_argument_invalid  enable value is invalid
 * @return status_no_device         Device not opened
 *
 *
 * Calls to this function are serialized with respect to other calls that 
 * alter the functionality of the device. This means that this function 
 * may block.
 */
int sis8300llrfdrv_set_ctrl_table_cnst_en(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned enable) {

    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    if ((ctrl_table != ctrl_table_sp) && (ctrl_table != ctrl_table_ff))
        return status_argument_invalid;

    status = sis8300_reg_read(
                sisdevice->handle, 
                table_cnst_en_addr[ctrl_table], 
                &ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    switch (enable) {
        case 0:
            ui32_reg_val &= ~(0x1 << 9);
            break;
        case 1:
            ui32_reg_val |= 0x1 << 9;
            break;
        default:
            pthread_mutex_unlock(&sisdevice->lock);
            return status_argument_invalid;
    }

    status = sis8300_reg_write(sisdevice->handle, 
                table_cnst_en_addr[ctrl_table], ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get the if table constant is enabled - setting is the same for I and Q 
 *        controller
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  ctrl_table   sp or ff 
 * @param [out] enable      0 or 1
 * 
 * @return status_success           Data retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 *
 * The function sets the rate at which next FF value is added to the 
 * PI-output. The setting is the same for both I and Q controller.
 */
int sis8300llrfdrv_get_ctrl_table_cnst_en(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table,
        unsigned *enable) {

    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    if ((ctrl_table != ctrl_table_sp) && (ctrl_table != ctrl_table_ff))
        return status_argument_invalid;

    status = sis8300_reg_read(sisdevice->handle, 
                table_cnst_en_addr[ctrl_table], &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    *enable = (unsigned) (ui32_reg_val & (0x1 << 9) ) >> 9 ;

    return status_success;
}



/**
 * @brief Set the FF/SP table for given PT
 *
 * @param [in] sisuser      User context struct
 * @param [in] ctrl_table   Either FF or SP, #sis8300llrfdrv_ctrl_table
 * @param [in] table        The actual table to be written
 * @param [in] nsamples     Size of the table
 *
 * @return status_success           The table was written
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 * @return status_argument_range    Value out of bounds
 *
 * The function performs no checks on table, but just copies a chunk of 
 * size nsamples (settable by #sis8300llrfdrv_set_ctrl_table_nelm)
 * pointed to by table pointer to the memory.
 *
 * The table consists of 32 bit elements, each element representing I 
 * and Q. The easiest way to handle the table is to interpret it as two
 * interleaved tables with 16 bit samples - one for I the other for Q,
 * where the first Q element is at offset 0 and I element at offset 1.
 * For conversion between raw 16 bit integers in the table and floats in
 * I and Q consult @see #sis8300llrfdrv_types.h
 *
 * This is a shadow register. To make the controller see new parameters,
 * a call to #sis8300llrfdrv_ctrl_table_update is needed.
 *
 * Calls to this function are serialized with respect to other calls that 
 * alter the functionality of the device. This means that this function 
 * may block.
 */
int sis8300llrfdrv_set_ctrl_table_raw(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned nelm, void *table_raw) {

    int status;
    uint32_t base_table_offset, last_table_offset;
    unsigned max_nelm;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    status = _sis8300llrfdrv_get_rw_ctrl_table_params(sisuser, 
                ctrl_table, &base_table_offset, &max_nelm);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status;
    }

    /* Check if the requested size is too big */
    if (nelm > max_nelm) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_range;
    }




    /* get base offset for control table */
    switch (ctrl_table) {
        case ctrl_table_sp:
            /* Get the last used memory*/
            status = sis8300_reg_read(sisdevice->handle, 
                        SIS8300LLRF_SP_STATUS_REG, 
                        &last_table_offset);
            
            if (status) {
                pthread_mutex_unlock(&sisdevice->lock);
                return status_device_access;
            }

            //select between the 2 memory slots for SP
            if (last_table_offset >> 16 == ((((uint32_t) sisdevice->mem_size) - SIS8300LLRFDRV_CTRL_TABLE_SIZE_MAX*2 ) << 1 ) >> 16)
                base_table_offset = ((uint32_t) sisdevice->mem_size) - ((uint32_t)(SIS8300LLRFDRV_CTRL_TABLE_SIZE_MAX*1.5));
            else
                base_table_offset = ((uint32_t) sisdevice->mem_size) - SIS8300LLRFDRV_CTRL_TABLE_SIZE_MAX*2;
            break;
        case ctrl_table_ff:
            /* Get the last used memory*/
            status = sis8300_reg_read(sisdevice->handle, 
                        SIS8300LLRF_FF_STATUS_REG, 
                        &last_table_offset);
            
            if (status) {
                pthread_mutex_unlock(&sisdevice->lock);
                return status_device_access;
            }

            //select between the 2 memory slots for FF
            if (last_table_offset >> 16 == ((((uint32_t) sisdevice->mem_size) - SIS8300LLRFDRV_CTRL_TABLE_SIZE_MAX ) << 1 ) >> 16)
                base_table_offset = ((uint32_t) sisdevice->mem_size) - SIS8300LLRFDRV_CTRL_TABLE_SIZE_MAX/2;
            else
                base_table_offset = ((uint32_t) sisdevice->mem_size) - SIS8300LLRFDRV_CTRL_TABLE_SIZE_MAX;
            break;
        default:
            pthread_mutex_unlock(&sisdevice->lock);
            return status_argument_invalid;
    }

    /* Write the table to ram */
    status = sis8300drv_write_ram_unlocked(sisdevice,
                (unsigned) base_table_offset,
                nelm * SIS8300LLRF_IQ_SAMPLE_BYTES,
                table_raw);

    /* set new base offset for control table */
    switch (ctrl_table) {
        case ctrl_table_sp:
            status = sis8300_reg_write(sisdevice->handle, 
                        SIS8300LLRF_MEM_CTRL_SP_BASE_REG, 
                        base_table_offset);
            
            if (status) {
                pthread_mutex_unlock(&sisdevice->lock);
                return status_device_access;
            }
            break;
        case ctrl_table_ff:
            status = sis8300_reg_write(sisdevice->handle, 
                        SIS8300LLRF_MEM_CTRL_FF_BASE_REG, 
                        base_table_offset);
            if (status) {
                pthread_mutex_unlock(&sisdevice->lock);
                return status_device_access;
            }
            break;
        default:
            pthread_mutex_unlock(&sisdevice->lock);
            return status_argument_invalid;
    }

    pthread_mutex_unlock(&sisdevice->lock);
    return status;
}

/**
 * @brief Read the control table from memory
 *
 * @param [in]  sisuser     User context struct
 * @param [in]  ctrl_table  Either FF or SP, #sis8300llrfdrv_ctrl_table
 * @param [in]  nsamples    Size of the table to read
 * @param [out] table       Will contain the table on success
 *
 * @return status_success           Information retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 *
 * Reads the SP or FF table from memory. It 
 * assumes that the table passed to the function is of size TABLE_SIZE 
 * (specified with #sis8300llrfdrv_set_ctrl_table_nelm). It does not 
 * perform any checks.
 *
 * The table consists of 32 bit elements, each element representing Q
 * and I. The easiest way to handle the table is to interpret it as two
 * interleaved tables with 16 bit samples - one for Q the other for I,
 * where the first Q element is at offset 0 and I element at offset 1.
 * For conversion between raw 16 bit integers in the table and floats in
 * I and Q consult @see #sis8300llrfdrv_types.h
 *
 * Calls to this function are serialized with respect to other calls that 
 * alter the functionality of the device. This means that this function 
 * may block.
 */
int sis8300llrfdrv_get_ctrl_table_raw(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned nelm, void *table_raw) {

    int status;
    uint32_t base_table_offset;
    unsigned max_nelm;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    status = _sis8300llrfdrv_get_rw_ctrl_table_params(sisuser, 
                ctrl_table,  &base_table_offset, &max_nelm);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status;
    }

    /* If the requested size is bigger than max table size just return
     * a chunk of max size */
    if (nelm > max_nelm) {
        nelm = max_nelm;
    }

    /* Read the table */
    status = sis8300drv_read_ram_unlocked(sisdevice,
                (unsigned) base_table_offset,
                nelm * SIS8300LLRF_IQ_SAMPLE_BYTES,
                table_raw);

    pthread_mutex_unlock(&sisdevice->lock);
    return status;
}





/* =================== INTERNAL LIBRARY FUNCTIONS =================== */

/**
 * @brief Helper function to retrieve parameters for control table
 *
 * @param [in]  sisuser           User context struct
 * @param [in]  ctrl_table        Table type, #sis8300llrfdrv_ctrl_table
 * @param [out] base_table_offset Will hold the offset of the table on 
 *                                success
 * @param [out] max_nsamples      Will hold max number of samples on 
 *                                success
 *
 * This is an internal library function, that should not be called from
 * client code.
 */
static inline int _sis8300llrfdrv_get_rw_ctrl_table_params(
                    sis8300drv_usr *sisuser, 
                    sis8300llrfdrv_ctrl_table ctrl_table,
                    uint32_t *base_table_offset, 
                    unsigned *max_nsamples) {

    int status;
	sis8300drv_dev *sisdevice = sisuser->device;

    /* get base offset for control table */
    switch (ctrl_table) {
        case ctrl_table_sp:
            status = sis8300_reg_read(sisdevice->handle, 
                        SIS8300LLRF_MEM_CTRL_SP_BASE_REG, 
                        base_table_offset);
            
            if (status) {
                return status_device_access;
            }
            break;
        case ctrl_table_ff:
            status = sis8300_reg_read(sisdevice->handle, 
                        SIS8300LLRF_MEM_CTRL_FF_BASE_REG, 
                        base_table_offset);
            if (status) {
                return status_device_access;
            }
            break;
        default:
            return status_argument_invalid;
    }

    return sis8300llrfdrv_get_ctrl_table_max_nelm(sisuser, 
                ctrl_table, max_nsamples);
}
