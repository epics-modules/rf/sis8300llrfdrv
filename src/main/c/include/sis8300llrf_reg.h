
/* m-kmod-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfdrv.h
 * @brief Header file for the sis8300 LLRF firmware user space library. 
 */

#ifndef SIS8300LLRFDRVREG_H_
#define SIS8300LLRFDRVREG_H_

#ifdef __cplusplus
extern "C" {
#endif

/* the following tags apply
 * _REG     Read + write reg
 * _R_REG   Read only reg
 * _W_REG   Write only reg
 * _C_REG   Clear reg
 * _S_REG   Set REg
 */

/* BASE
 * #define SIS8300LLRF_BASE 0x400
 */

/* Firmware Version */
#define SIS8300LLRF_FW_VER_R_REG                     0x500

/* ID */
#define SIS8300LLRF_ID_R_REG                         0x400
#define SIS8300LLRF_INST_ID_REG                      0x401

/* GOP */
#define SIS8300LLRF_GOP_REG                          0x402
#define SIS8300LLRF_GEN_STATUS_REG                       SIS8300LLRF_GOP_REG
#define SIS8300LLRF_GOP_MASK                             0x1f0 //last four bits represent FSM state. 
#define SIS8300LLRF_FSM_MASK                             0xf
#define SIS8300LLRF_GEN_STATUS_MASK                      SIS8300LLRF_GOP_MASK
#define SIS8300LLRF_GOP_PULSE_DONE_CNT_MASK              0xffff0000
#define SIS8300LLRF_GOP_PULSE_DONE_CNT_SHIFT             16

/*FSM*/
#define SIS8300LLRF_FSM_STATE_DETAIL_REG                0x50D

/* GIP */
#define SIS8300LLRF_GIP_REG                          0x403
#define SIS8300LLRF_GIP_S_REG                        0x404
#define SIS8300LLRF_GIP_C_REG                        0x405

#define SIS8300LLRF_GIP_PT_MASK                          0xffff0000
#define SIS8300LLRF_GIP_PT_SHIFT                         16
#define SIS8300LLRF_GIP_UPDATE_REASON_MASK               0x1e

#define SIS8300LLRF_GIP_SW_RESET_BIT                     0x400
#define SIS8300LLRF_GIP_FORCE_ILOCK_BIT                  0x2000

/* #define SIS8300LLRF_GIP_INIT_DONE_BIT                    0x001 
 */

/* PI control */
#define SIS8300LLRF_SP_CONFIG                       0x505
#define SIS8300LLRF_SP_STATUS_REG                   0x506
#define SIS8300LLRF_FF_CONFIG                       0x507
#define SIS8300LLRF_FF_STATUS_REG                   0x508

#define SIS8300LLRF_SP_TABLE_SPEED_REG              SIS8300LLRF_SP_CONFIG
#define SIS8300LLRF_SP_TABLE_CYCLIC_EN_REG                SIS8300LLRF_SP_CONFIG
#define SIS8300LLRF_SP_TABLE_CONSTANT_EN_REG              SIS8300LLRF_SP_CONFIG

#define SIS8300LLRF_FF_TABLE_SPEED_REG              SIS8300LLRF_FF_CONFIG
#define SIS8300LLRF_FF_TABLE_CYCLIC_EN_REG                SIS8300LLRF_FF_CONFIG
#define SIS8300LLRF_FF_TABLE_CONSTANT_EN_REG              SIS8300LLRF_FF_CONFIG

#define SIS8300LLRF_PI_1_K_REG                       0x406
#define SIS8300LLRF_PI_1_TS_DIV_TI_REG               0x407
#define SIS8300LLRF_PI_1_D_A2_REG                    0x511
#define SIS8300LLRF_PI_1_D_B0_REG                    0x512
#define SIS8300LLRF_PI_1_D_B1_REG                    0x513
#define SIS8300LLRF_PI_1_D_B2_REG                    0x514
#define SIS8300LLRF_PI_1_SAT_MAX_REG                 0x408
#define SIS8300LLRF_PI_1_SAT_MIN_REG                 0x409
#define SIS8300LLRF_PI_1_CTRL_REG                    0x40a
#define SIS8300LLRF_PI_1_FIXED_SP_REG                0x40b
#define SIS8300LLRF_PI_1_FIXED_FF_REG                0x40c

#define SIS8300LLRF_PI_2_K_REG                       0x40d
#define SIS8300LLRF_PI_2_TS_DIV_TI_REG               0x40e
#define SIS8300LLRF_PI_2_D_A2_REG                    0x515
#define SIS8300LLRF_PI_2_D_B0_REG                    0x516
#define SIS8300LLRF_PI_2_D_B1_REG                    0x517
#define SIS8300LLRF_PI_2_D_B2_REG                    0x518
#define SIS8300LLRF_PI_2_SAT_MAX_REG                 0x40f
#define SIS8300LLRF_PI_2_SAT_MIN_REG                 0x410
#define SIS8300LLRF_PI_2_CTRL_REG                    0x411
#define SIS8300LLRF_PI_2_FIXED_SP_REG                0x412
#define SIS8300LLRF_PI_2_FIXED_FF_REG                0x413

#define SIS8300LLRF_PI_CTRL_DELAY_REG                0x519

/* IQ control */
#define SIS8300LLRF_IQ_CTRL_REG                      0x414
#define SIS8300LLRF_IQ_ANGLE_REG                     0x415

/* VM control */
#define SIS8300LLRF_VM_ANGLE_OFFSET_REG              0x416
#define SIS8300LLRF_VM_CTRL_REG                      0x417
#define SIS8300LLRF_VM_MAG_LIMIT_REG                 0x418

/* MEASURED FREQUENCY SAMPLING REGISTER */
#define SIS8300LLRF_SAMPLING_CLK_FREQ_REG           0x509

/* CYCLES POS REGISTERS */
#define SIS8300LLRF_CYCLE_POS_AFTER_RFSTART_REG      0x50E
#define SIS8300LLRF_CYCLE_POS_BSTART_REG             0x520
#define SIS8300LLRF_CYCLE_POS_BEND_REG               0x521
#define SIS8300LLRF_CYCLE_POS_RFEND_REG              0x522
#define SIS8300LLRF_CYCLE_POS_ILOCK_REG              0x523

/* TRIGGER CONFIGURATION*/
#define SIS8300LLRFDRV_TRIGGER_RF_START_REG       0x538
#define SIS8300LLRFDRV_TRIGGER_BEAM_START_REG     0x539
#define SIS8300LLRFDRV_TRIGGER_BEAM_END_REG       0x53A
#define SIS8300LLRFDRV_TRIGGER_RF_END_REG         0x53B
#define SIS8300LLRFDRV_TRIGGER_PMS_REG            0x53C

/* LOOKUP TABLES */
#define SIS8300LLRF_LUT_CTRL_1_PARAM_REG             0x41c
#define SIS8300LLRF_LUT_CTRL_FF_NSAMPLES_REG             SIS8300LLRF_LUT_CTRL_1_PARAM_REG

#define SIS8300LLRF_LUT_CTRL_2_PARAM_REG             0x41d
#define SIS8300LLRF_LUT_CTRL_SP_NSAMPLES_REG             SIS8300LLRF_LUT_CTRL_2_PARAM_REG

#define SIS8300LLRF_LUT_CTRL_NSAMPLES_MASK              0x00ffffff

/* MEM CTRL PARAMS */
#define SIS8300LLRF_MEM_CTRL_1_PARAM_REG             0x41e
#define SIS8300LLRF_MEM_CTRL_FF_BASE_REG                 SIS8300LLRF_MEM_CTRL_1_PARAM_REG

#define SIS8300LLRF_MEM_CTRL_2_PARAM_REG             0x41f
#define SIS8300LLRF_MEM_CTRL_SP_BASE_REG                 SIS8300LLRF_MEM_CTRL_2_PARAM_REG

#define SIS8300LLRF_MEM_CTRL_4_PARAM_REG             0x421
#define SIS8300LLRF_MEM_CTRL_PI_ERR_BASE_REG             SIS8300LLRF_MEM_CTRL_4_PARAM_REG

#define SIS8300LLRF_MEM_CTRL_5_PARAM_REG             0x422
#define SIS8300LLRF_MEM_CTRL_CAV_IQ_BASE_REG             SIS8300LLRF_MEM_CTRL_5_PARAM_REG

#define SIS8300LLRF_MEM_CTRL_6_PARAM_REG             0x423
#define SIS8300LLRF_MEM_CTRL_PI_IN_BASE_REG              SIS8300LLRF_MEM_CTRL_6_PARAM_REG

#define SIS8300LLRF_MEM_CTRL_7_PARAM_REG             0x424
#define SIS8300LLRF_MEM_CTRL_PI_OUT_BASE_REG             SIS8300LLRF_MEM_CTRL_7_PARAM_REG

#define SIS8300LLRF_MEM_CTRL_8_PARAM_REG             0x425
#define SIS8300LLRF_MEM_CTRL_VM_OUT_BASE_REG             SIS8300LLRF_MEM_CTRL_8_PARAM_REG

/* PI ERR STATUS */
#define SIS8300LLRF_PI_ERR_CNT_R_REG               0x426

/* TRIGGERS */
#define SIS8300LLRF_BOARD_SETUP_REG                  0x427

/* NEAR IQ */
#define SIS8300LLRF_NEAR_IQ_1_PARAM_REG              0x428
#define SIS8300LLRF_NEAR_IQ_2_PARAM_REG              0x429
#define SIS8300LLRF_NEAR_IQ_DATA_W_REG               0x42a
#define SIS8300LLRF_NEAR_IQ_ADDR_REG                 0x42b

/* MISC */
#define SIS8300LLRF_16_FRAC_MASK                     0xffff0000
#define SIS8300LLRF_16_INT_MASK                      0x0000ffff

/* MODULATOR RIPPLE FILTER */
#define SIS8300LLRF_RIPPLE_FILTER_CTRL_REG           0x42e
#define SIS8300LLRF_RIPPLE_FILTER_CONSTA3_REG        0x530
#define SIS8300LLRF_RIPPLE_FILTER_CONSTA6_REG        0x531
#define SIS8300LLRF_RIPPLE_FILTER_CONSTB1_REG        0x532
#define SIS8300LLRF_RIPPLE_FILTER_CONSTB2_REG        0x533
#define SIS8300LLRF_RIPPLE_FILTER_CONSTB3_REG        0x534
#define SIS8300LLRF_RIPPLE_FILTER_CONSTB4_REG        0x535
#define SIS8300LLRF_RIPPLE_FILTER_CONSTB5_REG        0x536
#define SIS8300LLRF_RIPPLE_FILTER_CONSTB6_REG        0x537

//TODO: Maybe change to define just the first register
//and use a shift for the others
#define SIS8300LLRF_SIGMON_CH0_CFG_REG               0x550
#define SIS8300LLRF_SIGMON_CH1_CFG_REG               0x560
#define SIS8300LLRF_SIGMON_CH2_CFG_REG               0x570
#define SIS8300LLRF_SIGMON_CH3_CFG_REG               0x580
#define SIS8300LLRF_SIGMON_CH4_CFG_REG               0x590
#define SIS8300LLRF_SIGMON_CH5_CFG_REG               0x5A0
#define SIS8300LLRF_SIGMON_CH6_CFG_REG               0x5B0
#define SIS8300LLRF_SIGMON_CH7_CFG_REG               0x5C0
#define SIS8300LLRF_SIGMON_CH8_CFG_REG               0x5D0
#define SIS8300LLRF_SIGMON_CH9_CFG_REG               0x5E0

#define SIS8300LLRF_SIGMON_CH0_STATUS_REG            0x555
#define SIS8300LLRF_SIGMON_CH1_STATUS_REG            0x565
#define SIS8300LLRF_SIGMON_CH2_STATUS_REG            0x575
#define SIS8300LLRF_SIGMON_CH3_STATUS_REG            0x585
#define SIS8300LLRF_SIGMON_CH4_STATUS_REG            0x595
#define SIS8300LLRF_SIGMON_CH5_STATUS_REG            0x5A5
#define SIS8300LLRF_SIGMON_CH6_STATUS_REG            0x5B5
#define SIS8300LLRF_SIGMON_CH7_STATUS_REG            0x5C5
#define SIS8300LLRF_SIGMON_CH8_STATUS_REG            0x5D5
#define SIS8300LLRF_SIGMON_CH9_STATUS_REG            0x5E5

#define SIS8300LLRF_SIGMON_CH0_MAG_ANG_REG           0x553
#define SIS8300LLRF_SIGMON_CH1_MAG_ANG_REG           0x563
#define SIS8300LLRF_SIGMON_CH2_MAG_ANG_REG           0x573
#define SIS8300LLRF_SIGMON_CH3_MAG_ANG_REG           0x583
#define SIS8300LLRF_SIGMON_CH4_MAG_ANG_REG           0x593
#define SIS8300LLRF_SIGMON_CH5_MAG_ANG_REG           0x5A3
#define SIS8300LLRF_SIGMON_CH6_MAG_ANG_REG           0x5B3
#define SIS8300LLRF_SIGMON_CH7_MAG_ANG_REG           0x5C3
#define SIS8300LLRF_SIGMON_CH8_MAG_ANG_REG           0x5D3
#define SIS8300LLRF_SIGMON_CH9_MAG_ANG_REG           0x5E3

#define SIS8300LLRF_SIGMON_CH0_AVG_REG               0x554
#define SIS8300LLRF_SIGMON_CH1_AVG_REG               0x564
#define SIS8300LLRF_SIGMON_CH2_AVG_REG               0x574
#define SIS8300LLRF_SIGMON_CH3_AVG_REG               0x584
#define SIS8300LLRF_SIGMON_CH4_AVG_REG               0x594
#define SIS8300LLRF_SIGMON_CH5_AVG_REG               0x5A4
#define SIS8300LLRF_SIGMON_CH6_AVG_REG               0x5B4
#define SIS8300LLRF_SIGMON_CH7_AVG_REG               0x5C4
#define SIS8300LLRF_SIGMON_CH8_AVG_REG               0x5D4
#define SIS8300LLRF_SIGMON_CH9_AVG_REG               0x5E4

#define SIS8300LLRF_SIGMON_CH0_AVG_POS_REG           0x551
#define SIS8300LLRF_SIGMON_CH1_AVG_POS_REG           0x561
#define SIS8300LLRF_SIGMON_CH2_AVG_POS_REG           0x571
#define SIS8300LLRF_SIGMON_CH3_AVG_POS_REG           0x581
#define SIS8300LLRF_SIGMON_CH4_AVG_POS_REG           0x591
#define SIS8300LLRF_SIGMON_CH5_AVG_POS_REG           0x5A1
#define SIS8300LLRF_SIGMON_CH6_AVG_POS_REG           0x5B1
#define SIS8300LLRF_SIGMON_CH7_AVG_POS_REG           0x5C1
#define SIS8300LLRF_SIGMON_CH8_AVG_POS_REG           0x5D1
#define SIS8300LLRF_SIGMON_CH9_AVG_POS_REG           0x5E1

#define SIS8300LLRF_SIGMON_CH0_AVG_WIDTH_REG         0x552
#define SIS8300LLRF_SIGMON_CH1_AVG_WIDTH_REG         0x562
#define SIS8300LLRF_SIGMON_CH2_AVG_WIDTH_REG         0x572
#define SIS8300LLRF_SIGMON_CH3_AVG_WIDTH_REG         0x582
#define SIS8300LLRF_SIGMON_CH4_AVG_WIDTH_REG         0x592
#define SIS8300LLRF_SIGMON_CH5_AVG_WIDTH_REG         0x5A2
#define SIS8300LLRF_SIGMON_CH6_AVG_WIDTH_REG         0x5B2
#define SIS8300LLRF_SIGMON_CH7_AVG_WIDTH_REG         0x5C2
#define SIS8300LLRF_SIGMON_CH8_AVG_WIDTH_REG         0x5D2
#define SIS8300LLRF_SIGMON_CH9_AVG_WIDTH_REG         0x5E2

#define SIS8300LLRF_REFCOMP_POS_REG                  0x502
#define SIS8300LLRF_REFCOMP_WIDTH_REG                0x503
#define SIS8300LLRF_CAV_IQ_REG                       0x42F
#define SIS8300LLRF_REF_IQ_REG                       0x430


/* VM PRE-DISTORTION */
#define SIS8300LLRF_VM_PREDIST_R0_REG                0x43c
#define SIS8300LLRF_VM_PREDIST_R1_REG                0x43d
#define SIS8300LLRF_VM_PREDIST_DC_REG                0x43e
/* NOTCH FILTER */
#define SIS8300LLRF_NOTCH_FILTER_CONSTA_REG          0x43f
#define SIS8300LLRF_NOTCH_FILTER_CONSTB_REG          0x440
#define SIS8300LLRF_NOTCH_FILTER_CTRL_REG            0x441
/* LOW PASS */
#define SIS8300LLRF_IQ_LP_A_REG                      0x442
#define SIS8300LLRF_IQ_LP_B_REG                      0x443
#define SIS8300LLRF_IQ_LP_CTRL_REG                   0x444
/* INNER-LOOP */
#define SIS8300LLRF_IL_LP_COEFF_REG                  0x445
#define SIS8300LLRF_IL_PI_PARAM_REG                  0x446
#define SIS8300LLRF_IL_PI_I_START_REG                0x447
#define SIS8300LLRF_IL_PHASE_CTRL_REG                0x448

/* LINEARISATION */
#define SIS8300LLRF_LIN_LUT_ADDR                     0x449
#define SIS8300LLRF_LIN_LUT_DATA                     0x44a


/* AUXILIARY CHANNELS -> DOWNSAMPLD AND ITERNAL*/
#define SIS8300LLRF_AUX_CH_OFFSET                      0x10  //offset between channel registers

/* DOWNSAMPLED CHANNELS */
#define SIS8300LLRF_DWNSMPL_CH0_CFG_REG              0x600 //configuration register for channel 0
#define SIS8300LLRF_DWNSMPL_CH0_ADDR_REG             0x602 // memory address from first channel
#define SIS8300LLRF_DWNSMPL_CH0_BYTE_COUNT_REG       0x603 // Number of bytes acquired by channel 0
#define SIS8300LLRF_DSACQ_CH0_SAMPLES_REG            0x608 // Number of samples to acquire
#define SIS8300LLRF_DWNSMPL_CH0_DECM_REG             0x604 // Decimation register

#define SIS8300LLRF_DWNSMPL_CH0_DAQ_FMT_REG          0x550 //  DAQ format selection
                                                           // Mag/Ang, IQ or DC
/* INTERNAL CHANNELS */
#define SIS8300LLRF_INTERN_CH0_CFG_REG              0x6A0 //configuration register for channel 0
#define SIS8300LLRF_INTERN_CH0_ADDR_REG             0x6A2 // memory address from first channel
#define SIS8300LLRF_INTERN_CH0_BYTE_COUNT_REG       0x6A3 // Number of bytes acquired by channel 0
#define SIS8300LLRF_INTACQ_CH0_SAMPLES_REG          0x6A8 // Number of samples to acquire
#define SIS8300LLRF_INTERN_CH0_DECM_REG             0x6A4 // Decimation register
#define SIS8300LLRF_INTERN_PI_ERROR_REG             0x504 // DAQ format for PI ERROR

/* CTRL INPUT SEL*/
#define SIS8300LLRF_CTRL_INPUT_SEL_REG              0x501 //Select input controller

/* ILOCK REGISTERS*/
#define SIS8300LLRF_ILOCK_CAUSE                      SIS8300LLRF_FSM_STATE_DETAIL_REG
#define SIS8300LLRF_ILOCK_CAUSE_MASK                 0x7 

/* DAQ SETUP / INFORMATION */
#define SIS8300LLRF_MAX_RF_DURATION_REG              0x50A
#define SIS8300LLRF_MAX_DAQ_DURATION_REG             0x50B

#define SIS8300LLRF_RFEND_CAUSE_REG                  SIS8300LLRF_FSM_STATE_DETAIL_REG
#define SIS8300LLRF_RFEND_CAUSE_MASK                 0x18
#define SIS8300LLRF_RFEND_CAUSE_SHIFT                0x3 

#define SIS8300LLRF_DAQEND_CAUSE_REG                 SIS8300LLRF_FSM_STATE_DETAIL_REG
#define SIS8300LLRF_DAQEND_CAUSE_MASK                 0x20
#define SIS8300LLRF_DAQEND_CAUSE_SHIFT                0x5 

/* RFLPS ILOCK RESET */
#define SIS8300LLRF_LPS_ILOCK_1_REG                   0xF05
#define SIS8300LLRF_LPS_ILOCK_2_REG                   0xF06

#define SIS8300LLRF_LPS_DEAD_TIME_REG                   0x50C

#ifdef __cplusplus
}
#endif

#endif /* SIS8300LLRFDRVREG_H_ */
