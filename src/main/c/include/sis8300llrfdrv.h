/*
 * m-kmod-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfdrv.h
 * @brief Header file for the sis8300 LLRF firmware user space library. 
 */

#ifndef SIS8300LLRFDRV_H_
#define SIS8300LLRFDRV_H_
#include <stdint.h>

#define SIS8300LLRFDRV_DEBUG

#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>

#if UINT_MAX == 0xffffffff
#else
#error UNSIGNED DATA TYPE IS NOT 4 BYTES UINT_MAX
#endif


#undef SIS8300LLRF_PDEBUG
#ifdef SIS8300LLRF_DEBUG
#include <stdio.h>
#define SIS8300LLRF_PDEBUG(fmt, args...) printf("sis8300llrfdrv DEBUG:" fmt, ## args)
#else
#define SIS8300LLRF_PDEBUG(fmt, args...)
#endif

#define SIS8300LLRF_INFO(fmt, args...) printf("sis8300llrfdrv INFO:" fmt, ## args)

/* Block bytes check for write ram */
#define SIS8300LLRF_IQ_SAMPLE_BYTES      4
#define SIS8300LLRF_MEM_CTRL_BLOCK_BYTES 64

/*
#if SIS8300LLRF_MEM_CTRL_BLOCK_BYTES == SIS8300DRV_BLOCK_BYTES
#else
#error: SIS8300LLRFDRV_MEM_CTRL_BLOCK_BYTES not the same as SIS8300DRV_BLOCK_BYTES
#endif
*/


#define SIS8300LLRFDRV_HW_ID                    0xB00B  /** < Unique LLRF controller ID */

#define SIS8300LLRFDRV_FW_VERSION_MAJOR_MA      0x01    /** < Magnitude Angle based fw version of the controller, fw major revision */
#define SIS8300LLRFDRV_FW_VERSION_MAJOR_IQ      0x02    /** < IQ based fw version of the controller, fw major revision */

/* meaning of analogue input channels */
#define SIS8300LLRFDRV_AI_CHAN_CAV              0   /** < AI channel 0 always corresponds to cavity probe input */
#define SIS8300LLRFDRV_AI_CHAN_REF              1   /** < AI channel 1 always corresponds to reference input */

/* GOP = General Output register status bits */
#define SIS8300LLRFDRV_GEN_STATUS_VM_MAG_LIMITER_ACTIVE 0x100   /**< VM output limiter is active and constrining ouput to
                                                                   SIS8300LLRFDRV_VM_MAG_LIMIT */
#define SIS8300LLRFDRV_GEN_STATUS_PI_OVERFLOW_I         0x80    /**< Overflow in PI Ctrl for I controller */
#define SIS8300LLRFDRV_GEN_STATUS_PI_OVERFLOW_Q         0x40    /**< Overflow in PI Ctrl for Q controller */
#define SIS8300LLRFDRV_GEN_STATUS_READ_ERR              0x20    /**< Read error while accessing register */
#define SIS8300LLRFDRV_GEN_STATUS_WRITE_ERR             0x10    /**< Write error while accessing register */
#define SIS8300LLRFDRV_GEN_STATUS_PMS_ACT               0x08    /**< PMS active */

/* Update reasons, to be used with #sis8300llrfdrv_update */
#define SIS8300LLRFDRV_UPDATE_REASON_CTRL_TBL           0x008   /**< Set this to get the controller to reload the controller tables, when calling #sis8300llrfdrv_update */
#define SIS8300LLRFDRV_UPDATE_REASON_NEW_PT             0x004   /**< Set this to get the controller to load new FF and SP tables (corresponding to new pt) from memory, when calling #sis8300llrfdrv_update */
#define SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS         0x002   /**< Set this to get the controller to use new parameters, when calling #sis8300llrfdrv_update */
#define SIS8300LLRFDRV_UPDATE_REASON_INIT_DONE          0x001   /**< Set this to get the controller to reload everything, when calling #sis8300llrfdrv_update */


#define SIS8300LLRFDRV_STATUS_CLR_GENERAL               1       /** < Set this to clear general latched statuses, when calling #sis8300llrfdrv_clear_latched_statuses */
#define SIS8300LLRFDRV_STATUS_CLR_SIGMON                2       /** < Set this to clear signal monitor latched statuses, when calling #sis8300llrfdrv_clear_latched_statuses */

#define SIS8300LLRFDRV_CTRL_TABLE_NELM_MAX              0x1000000 /** < Max nelm size of control tables */
#define SIS8300LLRFDRV_CTRL_TABLE_CIRC_NELM_MIN              0x28 /** < Minimum size for table when in circular mode */


/*The value should be converted to bytes, and is multiplied by 2 because
 * is I and Q components interleaved 
 * and multiplied by 2 again because we use 2 memory region per each table,
 * so when a new table is written it will not overwrite the old one*/
#define SIS8300LLRFDRV_CTRL_TABLE_SIZE_MAX              SIS8300LLRFDRV_CTRL_TABLE_NELM_MAX * SIS8300LLRF_IQ_SAMPLE_BYTES * 2 * 2

#define SIS8300LLRFDRV_MON_AVGWIDTH_MAX                 8388608   /** < Max size for average width for signal monitoring and reference compensation monitoring*/

#define SIS8300LLRFDRV_MAX_CH_CTRL_INPUT_SEL            7   /** < Maximum channel that could be set as controller input*/

/* ==================================================== */
/* ================ Basic information ================= */
/* ==================================================== */
int sis8300llrfdrv_get_fw_version(sis8300drv_usr *sisuser, unsigned *ver_device, unsigned *ver_major, unsigned *ver_minor);
int sis8300llrfdrv_get_fw_id(sis8300drv_usr *sisuser, unsigned *fwDevice);

int sis8300llrfdrv_get_sw_id(sis8300drv_usr *sisuser, unsigned *sw_id);
int sis8300llrfdrv_set_sw_id(sis8300drv_usr *sisuser, unsigned sw_id);

/* ==================================================== */
/* ================ Initialization ==================== */
/* ==================================================== */
int sis8300llrfdrv_setup_dac(sis8300drv_usr *sisuser);
int sis8300llrfdrv_setup_adc_tap_delay(sis8300drv_usr *sisuser);
int sis8300llrfdrv_mem_ctrl_set_custom_mem_map(sis8300drv_usr *sisuser);
int sis8300llrfdrv_enable_backplane_trigger(sis8300drv_usr *sisuser);

/* ==================================================== */
/* ============ Triggers and Interlocks =============== */
/* ==================================================== */
/**
 * @brief Trigger setup
 *
 * Enumerator of trigger setups for PULSE_COMMING, PULSE_START, PULSE_END,
 * PMS trigger is always on mlvds3,7 and harlink4 (high on any oif these outputs will
 * cause the trigger)
 */
typedef enum {
    mlvds_012 = 0,  /**< PULSE_COMMING on mlvds0, PULSE_START on mlvds1, PULSE_END on mlvds2 */
    mlvds_456 = 1   /**< PULSE_COMMING on mlvds4, PULSE_START on mlvds5, PULSE_END on mlvds6 */
} sis8300llrfdrv_trg_setup;
int sis8300llrfdrv_set_trigger_setup(sis8300drv_usr *sisuser, sis8300llrfdrv_trg_setup trg_setup);
int sis8300llrfdrv_get_trigger_setup(sis8300drv_usr *sisuser, sis8300llrfdrv_trg_setup *trg_setup);

/*@ brief get Interlock cause*/
typedef enum {
    ilock_no_interlock = 0,
    ilock_register_force = 1,
    ilock_signal_monitorig = 2,
    ilock_lps = 3,
    ilock_pms = 4,
    ilock_sys_prep_timeout = 5
} sis8300llrfdrv_ilock_cause;

int sis8300llrfdrv_get_ilock_cause(sis8300drv_usr *sisuser, 
        unsigned *ilock_cause);


int sis8300llrfdrv_set_dead_time_soft_lps(sis8300drv_usr *sisuser, uint32_t dead_time);

int sis8300llrfdrv_get_dead_time_soft_lps(sis8300drv_usr *sisuser, uint32_t *dead_time);

int sis8300llrfdrv_force_ilock(sis8300drv_usr *sisuser);

int sis8300llrfdrv_set_signal_from_lps(sis8300drv_usr *sisuser, unsigned enable);
/**
 * @brief Interlock condition
 *
 * Enumerator for all the available ILOCK conditions, an active ILOCK will trigger
 * PMS in custom logic. This will happen only once after activation of ILOCK
 *
 * IMPORTAINT: a call to #sis8300llrfdrv_sw_reset will disable level sensitive ILOCK,
 * which will allow the controller to run again after a PMS, even though the HW ILOCK
 * might still be active.
 */
typedef enum {
    ilock_disabled     = 0,
    ilock_rising_edge  = 1,
    ilock_falling_edge = 2,
    ilock_high_level   = 3,
    ilock_low_level    = 4
} sis8300llrfdrv_ilock_condition;
int sis8300llrfdrv_set_ilock_condition(sis8300drv_usr *sisuser, unsigned harl_inp, sis8300llrfdrv_ilock_condition condition);
int sis8300llrfdrv_get_ilock_condition(sis8300drv_usr *sisuser, unsigned harl_inp, sis8300llrfdrv_ilock_condition *condition);

/* ==================================================== */
/* ================= Controller status rw ============= */
/* ==================================================== */
/**
 * @brief GOP - General Output register bits
 *
 * Value of each element corresponds to bit that represents the
 * element in the register (except for status_all).
 */
typedef enum {
    gen_status_write_error = 4,          /** < write error status */
    gen_status_read_error = 5,           /** < read error status */
    gen_status_pi_overflow_I = 6,        /** < PI overflow in Q part */
    gen_status_pi_overflow_Q = 7,        /** < PI overflow in I part */
    gen_status_vm_mag_limiter_active = 8 /** VM output limiter is active and constraining output to SIS8300LLRFDRV_VM_MAG_LIMIT */
} sis8300llrfdrv_gen_status_bit;
int sis8300llrfdrv_get_general_status(sis8300drv_usr *sisuser, sis8300llrfdrv_gen_status_bit gen_status_bit, unsigned *gen_status);

typedef enum {
    fsm_state_error = 0,
    fsm_state_reset = 1,
    fsm_state_init = 2,
    fsm_state_idle = 3,
    fsm_state_load_ctrl_tables = 4,
    fsm_state_active_pre_beam = 5,
    fsm_state_active_beam = 6,
    fsm_state_active_post_beam = 7,
    fsm_state_daq_post_rf = 8,
    fsm_state_lps_daq = 9,
    fsm_state_lps_wait = 10,
    fsm_state_softilock_daq = 11,
    fsm_state_interlock_daq = 12,
    fsm_state_interlock_soft = 13,
    fsm_state_interlock = 14
} sis8300llrfdrv_fsm_state;

int sis8300llrfdrv_get_fsm_state(sis8300drv_usr *sisuser, unsigned *fsm_state);


/**
 * @brief Enumerator of signal monitor statuses
 */
typedef enum {
    sigmon_stat_ilock = 0,
    sigmon_stat_pms   = 1,
    sigmon_stat_alarm = 2,
    sigmon_stat_alarm_not_latched= 3
} sis8300llrfdrv_sigmon_stat;

int sis8300llrfdrv_get_sigmon_status(sis8300drv_usr *sisuser, sis8300llrfdrv_sigmon_stat status_select, int chan, unsigned *status_val);

int sis8300llrfdrv_clear_latched_statuses(sis8300drv_usr *sisuser, unsigned what); /* TODO: rename */

int sis8300llrfdrv_get_pulse_done_count(sis8300drv_usr *sisuser, unsigned *pulse_count);
int sis8300llrfdrv_clear_pulse_done_count(sis8300drv_usr *sisuser);

int sis8300llrfdrv_update(sis8300drv_usr *sisuser, unsigned update_reason);
int sis8300llrfdrv_update_armed(sis8300drv_usr *sisuser, unsigned update_reason);
int sis8300llrfdrv_init_done(sis8300drv_usr *sisuser);
int sis8300llrfdrv_sw_reset(sis8300drv_usr *sisuser);

/* ==================================================== */
/* =============== Control tables ===================== */
/* ==================================================== */
/**
 * @brief Control table types
 */
typedef enum {
    ctrl_table_ff      = 0, /**< Set Point control table */
    ctrl_table_sp      = 1, /**< Feed forward control table */
    ctrl_table_invlaid = 2
} sis8300llrfdrv_ctrl_table;

typedef enum {
    table_mode_normal   = 0,
    table_mode_circular = 1
} sis8300llrfdrv_table_mode;
int sis8300llrfdrv_set_table_mode(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_ctrl_table ctrl_table,
        sis8300llrfdrv_table_mode mode);
int sis8300llrfdrv_get_table_mode(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_ctrl_table ctrl_table,
        sis8300llrfdrv_table_mode *mode);

int sis8300llrfdrv_get_ctrl_table_max_nelm(sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, uint32_t *max_nelm);

int sis8300llrfdrv_set_ctrl_table_nelm(sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, uint32_t nelm);
int sis8300llrfdrv_get_ctrl_table_nelm(sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, uint32_t *nelm);

int sis8300llrfdrv_set_ctrl_table_speed(sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, unsigned speed);
int sis8300llrfdrv_get_ctrl_table_speed(sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, unsigned *speed);

int sis8300llrfdrv_set_ctrl_table_cnst_en(
sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, unsigned enable);
int sis8300llrfdrv_get_ctrl_table_cnst_en(
sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, unsigned *enable);

int sis8300llrfdrv_set_ctrl_table_raw(sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, unsigned nelm, void *table_raw);
int sis8300llrfdrv_get_ctrl_table_raw(sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, unsigned nelm, void *table_raw);

/* ==================================================== */
/* ================== ACQUISITION ===================== */
/* ==================================================== */
int sis8300llrfdrv_arm_device(sis8300drv_usr *sisuser);
int sis8300llrfdrv_wait_pulse_done_pms(sis8300drv_usr *sisuser, unsigned timeout);


/**
 * @brief Cycle and samples counts readbacks
 */
typedef enum {
    samples_cnt_pi_total        = 0,
    cycles_pos_after_rfstart    = 1,
    cycles_pos_bstart           = 2,
    cycles_pos_bend             = 3,
    cycles_pos_rfend            = 4,
    cycles_pos_ilock            = 5
} sis8300llrfdrv_cycles_samples_cnt; 

#define SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM 6
int sis8300llrfdrv_get_cycles_samples_cnt(sis8300drv_usr *sisuser, sis8300llrfdrv_cycles_samples_cnt source, unsigned *cnt_val);

int sis8300llrfdrv_get_acquired_nsamples_aux(sis8300drv_usr *sisuser, int down_or_intern, unsigned channel, unsigned *samples_cnt_val);
int sis8300llrfdrv_get_pi_err_max_nsamples(sis8300drv_usr *sisuser, unsigned *max_nsamples);
int sis8300llrfdrv_update_mem_position_aux_channel(sis8300drv_usr *sisuser);
int sis8300llrfdrv_read_aux_channel(sis8300drv_usr *sisuser, unsigned channel, unsigned down_or_intern, void *raw_data, unsigned nsamples);


/* ==================================================== */
/* =============== PARAMTER SETUPS ==================== */
/* ==================================================== */
/* Below are paramer setters and getters for:
 *              * PI Cotnroller
 *              * Modulator Ripple Filter
 *              * Non IQ Sampling
 *              * Vector Modulator
 *              * Signal Monitoring
 * They all work in the same principle of having an
 * enumerator of possible paramters and than a getter
 * and a setter which take/return a double. When setting
 * a param they provide also the rounding error because
 * the paramter needs to be converted to fixed point.
 * If it is an integer param, the error is always zero  */
/* ==================================================== */
/* ==================================================== */


/* =============== PI CONTROLLER SETUP ================ */

/**
 * @brief PI instance enumerator
 */
typedef enum {
    pi_I   = 0, /** < Q PI instance */
    pi_Q   = 1  /** < I PI instance */
} sis8300llrfdrv_pi_type;

/**
 * @brief Enumerator of parameters/settings required to setup the PI controller.
 */
typedef enum {
    pi_param_gain_K        = 0,  /**< PI controller K gain */
    pi_param_gain_TSdivTI  = 1,  /**< PI controller ts/ti gain */
    pi_param_D_a2          = 2, 
    pi_param_D_b0          = 3, 
    pi_param_D_b1          = 4, 
    pi_param_D_b2          = 5, 
    pi_param_sat_max       = 6,  /**< Saturation max value */
    pi_param_sat_min       = 7,  /**< Saturation min value */
    pi_param_fixed_sp_val  = 8,  /**< PI Controller Fixed SP val */
    pi_param_fixed_ff_val  = 9,  /**< PI Controller Fixed FF val */
    pi_param_sel_out_src   = 10   /**< Select output source */
} sis8300llrfdrv_pi_param;
#define SIS8300LLRFDRV_PI_PARAM_INT_FIRST pi_param_sel_out_src
#define SIS8300LLRFDRV_PI_PARAM_NUM       11

int sis8300llrfdrv_set_pi_param(sis8300drv_usr *sisuser, sis8300llrfdrv_pi_type pi, sis8300llrfdrv_pi_param pi_param, double param_val, double *param_err);
int sis8300llrfdrv_get_pi_param(sis8300drv_usr *sisuser, sis8300llrfdrv_pi_type pi, sis8300llrfdrv_pi_param pi_param, double *param_val);
int sis8300llrfdrv_set_pid_delay_rfstart(sis8300drv_usr *sisuser, double delay, double *param_err);
int sis8300llrfdrv_get_pid_delay_rfstart(sis8300drv_usr *sisuser, double *delay);
/**
 * @brief loop mode
 */
typedef enum {
    closed_loop = 0,
    open_loop = 1
} sis8300llrfdrv_loop_mode;

int sis8300llrfdrv_set_loop_mode(sis8300drv_usr *sisuser, sis8300llrfdrv_loop_mode mode);
int sis8300llrfdrv_get_loop_mode(sis8300drv_usr *sisuser, sis8300llrfdrv_loop_mode* mode);

/* =============== Modulator Ripple Filter setup ============= */
/**
 * @brief Enumerator of modulator ripple filter paramters/settings
 */
typedef enum {
    ripple_fil_const_a3      = 0,    /**< Modulator Ripple Filter constant A3 */
    ripple_fil_const_a6      = 1,    /**< Modulator Ripple Filter Constant A6 */
    ripple_fil_const_b1      = 2,    /**< Modulator Ripple Filter Constant B1 */
    ripple_fil_const_b2      = 3,    /**< Modulator Ripple Filter Constant B2 */
    ripple_fil_const_b3      = 4,    /**< Modulator Ripple Filter Constant B3 */
    ripple_fil_const_b4      = 5,    /**< Modulator Ripple Filter Constant B4 */
    ripple_fil_const_b5      = 6,    /**< Modulator Ripple Filter Constant B5 */
    ripple_fil_const_b6      = 7,    /**< Modulator Ripple Filter Constant B6 */
    /* integer params */
    /* enable or disable flags */
    ripple_fil_en            = 8,    /**< Enable Modulator Ripple Filter */
} sis8300llrfdrv_ripple_filter_param;
#define SIS8300LLRFDRV_RIPPLE_FILTER_PARAM_INT_FIRST   ripple_fil_en
#define SIS8300LLRFDRV_RIPPLE_FILTER_PARAM_NUM         9

int sis8300llrfdrv_set_ripple_filter_param(sis8300drv_usr *sisuser, sis8300llrfdrv_ripple_filter_param param, double param_val, double *param_err);
int sis8300llrfdrv_get_ripple_filter_param(sis8300drv_usr *sisuser, sis8300llrfdrv_ripple_filter_param param, double *param_val);


/* =============== Notch Filter setup ============= */
/**
 * @brief Enumerator of notch filter paramters/settings
 */
typedef enum {
    notch_fil_const_a_real      = 0,    /**< Notch Filter constant a real part */
    notch_fil_const_a_imag      = 1,    /**< Notch Filter constant a imaginary part */
    notch_fil_const_b_real      = 2,    /**< Notch Filter constant b real part */
    notch_fil_const_b_imag      = 3,    /**< Notch Filter constant b imaginary part */
    /* integer params */
    /* enable or disable flags */
    notch_fil_en                = 4     /**< Enable Notch Filter */
} sis8300llrfdrv_notch_filter_param;
#define SIS8300LLRFDRV_NOTCH_FILTER_PARAM_INT_FIRST   notch_fil_en
#define SIS8300LLRFDRV_NOTCH_FILTER_PARAM_NUM         5

int sis8300llrfdrv_set_notch_filter_param(sis8300drv_usr *sisuser, sis8300llrfdrv_notch_filter_param param, double param_val, double *param_err);
int sis8300llrfdrv_get_notch_filter_param(sis8300drv_usr *sisuser, sis8300llrfdrv_notch_filter_param param, double *param_val);

/* =============== Low Pass Filter setup ============= */
/**
 * @brief Enumerator of low pass filter paramters/settings
 */
typedef enum {
    low_pass_fil_const_a      = 0,    /**< Low Pass Filter constant a real part */
    low_pass_fil_const_b      = 1,    /**< Low Pass Filter constant b real part */
    /* integer params */
    /* enable or disable flags */
    low_pass_fil_en                = 2     /**< Enable Low Pass Filter */
} sis8300llrfdrv_low_pass_filter_param;
#define SIS8300LLRFDRV_LOW_PASS_FILTER_PARAM_INT_FIRST   low_pass_fil_en
#define SIS8300LLRFDRV_LOW_PASS_FILTER_PARAM_NUM         3

int sis8300llrfdrv_set_low_pass_filter_param(sis8300drv_usr *sisuser, sis8300llrfdrv_low_pass_filter_param param, double param_val, double *param_err);
int sis8300llrfdrv_get_low_pass_filter_param(sis8300drv_usr *sisuser, sis8300llrfdrv_low_pass_filter_param param, double *param_val);

/* =============== Vector Modulator =============== */
/**
 * @brief Enumerator of Vecotrm Modulator parameters/settings
 */
typedef enum {
    vm_param_angleoffset	= 0,
    vm_param_mag_lim_val        = 1,
    vm_param_predist_rc00       = 2,
    vm_param_predist_rc01       = 3,
    vm_param_predist_rc10       = 4,
    vm_param_predist_rc11       = 5,
    vm_param_predist_dcoffset_i = 6,
    vm_param_predist_dcoffset_q = 7,
    /* enable or disable flags */
    vm_param_inverse_q_en       = 8,
    vm_param_inverse_i_en       = 9,
    vm_param_mag_lim_en         = 10,
    vm_param_swap_iq            = 11,
    vm_param_force_mag          = 12,
    vm_param_force_ang          = 13,
    vm_param_predistort_en      = 14,
    vm_param_ang_off_en         = 15,
    vm_param_inn_loop_en        = 16,
    vm_param_lin_lut_en         = 17
} sis8300llrfdrv_vm_param;
#define SIS8300LLRFDRV_VM_PARAM_INT_FIRST   vm_param_inverse_q_en
#define SIS8300LLRFDRV_VM_PARAM_NUM         18

int sis8300llrfdrv_set_vm_param(sis8300drv_usr *sisuser, sis8300llrfdrv_vm_param param, double param_val, double *param_err);
int sis8300llrfdrv_get_vm_param(sis8300drv_usr *sisuser, sis8300llrfdrv_vm_param param, double *param_val);

/* =============== Non IQ sampling =============== */
/**
 * @brief enumerator of non-IQ sampling parameters
 */
typedef enum {
    iq_param_angle_offset_val    = 0,
    /* integer params */
    iq_param_cav_inp_delay_val   = 1,
	/* enable or disable flags */
    iq_param_angle_offset_en     = 2,
    iq_param_cav_inp_delay_en	 = 3
}sis8300llrfdrv_iq_param;
#define SIS8300LLRFDRV_IQ_PARAM_INT_FIRST   iq_param_cav_inp_delay_val
#define SIS8300LLRFDRV_IQ_PARAM_NUM         4

int sis8300llrfdrv_set_iq_param(sis8300drv_usr *sisuser, sis8300llrfdrv_iq_param param, double param_val, double *param_err);
int sis8300llrfdrv_get_iq_param(sis8300drv_usr *sisuser, sis8300llrfdrv_iq_param param, double *param_val);

int sis8300llrfdrv_set_near_iq(sis8300drv_usr *sisuser, unsigned M, unsigned N);
int sis8300llrfdrv_get_near_iq(sis8300drv_usr *sisuser, unsigned *M, unsigned *N);

/* =================== Signal Monitoring ============== */
/**
 * @brief Eumerator of signal monitoring paramters
 */
typedef enum {
    sigmon_treshold       = 0,
    /* integer params */
    sigmon_start_evnt     = 1,
    sigmon_end_evnt       = 2,
    /* enable or disable flags */
    sigmon_alarm_cnd       = 3,
    sigmon_pms_en     	  = 4,
    sigmon_ilock_en   	  = 5,
    sigmon_win_pos        = 6,
    sigmon_win_width      = 7
} sis8300llrfdrv_sigmon_param;
#define SIS8300LLRFDRV_SIGMON_PARAM_INT_FIRST sigmon_start_evnt
#define SIS8300LLRFDRV_SIGMON_PARAM_NUM       8
#define SIS8300LLRFDRV_SIGMON_PARAM_WIN_FIRST sigmon_win_pos

int sis8300llrfdrv_set_sigmon_param(sis8300drv_usr *sisuser, sis8300llrfdrv_sigmon_param param, int chan, double param_val, double *param_err);
int sis8300llrfdrv_get_sigmon_param(sis8300drv_usr *sisuser, sis8300llrfdrv_sigmon_param param, int chan, double *param_val);
int sis8300llrfdrv_get_sigmon_mag_ang(sis8300drv_usr *sisuser, int chan, double *mag, double* ang);
int sis8300llrfdrv_get_sigmon_mag_minmax(sis8300drv_usr *sisuser, int chan, double *mag_minmax_val);
int sis8300llrfdrv_get_sigmon_mag_avg(sis8300drv_usr *sisuser, int chan, double *avg_mag_val);


/* =================== DAQ Information ============== */

int sis8300llrfdrv_set_max_rf_length(sis8300drv_usr *sisuser, uint32_t max_rf_length);
int sis8300llrfdrv_get_max_rf_length(sis8300drv_usr *sisuser, uint32_t *max_rf_length);

int sis8300llrfdrv_set_max_daq_length(sis8300drv_usr *sisuser, uint32_t max_daq_length);
int sis8300llrfdrv_get_max_daq_length(sis8300drv_usr *sisuser, uint32_t *max_daq_length);

/**
 * @brief Eumerator of rf end causes
 */
typedef enum {
    rfend_cause_normal      = 0,
    rfend_cause_timer       = 1,
    rfend_cause_premature   = 2,
} sis8300llrfdrv_rfend_cause;

int sis8300llrfdrv_get_rfend_cause(sis8300drv_usr *sisuser, unsigned *rfend_cause);

/**
 * @brief Eumerator of daq end causes
 */
typedef enum {
    daqend_cause_normal      = 0,
    daqend_cause_timer       = 1,
} sis8300llrfdrv_daqend_cause;

int sis8300llrfdrv_get_daqend_cause(sis8300drv_usr *sisuser, unsigned *daqend_cause);


/* =================== Reference Compensation Monitoring ============== */
/**
 * @brief Eumerator of reference compensation monitoring paramters
 */
typedef enum {
    refcomp_win_pos        = 0,
    refcomp_win_width      = 1
} sis8300llrfdrv_refcomp_param;
#define SIS8300LLRFDRV_REFCOMP_PARAM_INT_FIRST refcomp_win_pos
#define SIS8300LLRFDRV_REFCOMP_PARAM_NUM       2

int sis8300llrfdrv_set_refcomp_param(sis8300drv_usr *sisuser, sis8300llrfdrv_refcomp_param param, double param_val, double *param_err);
int sis8300llrfdrv_get_refcomp_param(sis8300drv_usr *sisuser, sis8300llrfdrv_refcomp_param param, double *param_val);
int sis8300llrfdrv_get_refcomp_iq_avg(sis8300drv_usr *sisuser, int ctrl_or_ref, double *i, double* q);


/* =============== AUXILIARY CHANNELS SETUP ================ */
/* Auxiliary channels are down-sampled or internal channel   */

/**
 * @brief Enumerator of parameters/settings required to setup the downsampled channels.
 */
typedef enum {
    aux_param_enable          = 0, /*< enable/disable this channel**/
    aux_param_samples_cnt     = 1, /*< number of samples to be acquired**/
    aux_param_dec_enable      = 2, /*< enable decimation**/
    aux_param_dec_factor      = 3, /*< decimation factor**/
    aux_param_daq_fmt         = 4, /*< select Mag/Ang, IQ or DC format, just
                                    used by down-sampled channels**/
} sis8300llrfdrv_aux_param;
#define SIS8300LLRFDRV_AUX_PARAM_INT_FIRST aux_param_enable
#define SIS8300LLRFDRV_AUX_PARAM_NUM            5
#define SIS8300LLRFDRV_DWNSMPL_CHANNELS        10
#define SIS8300LLRFDRV_INTERN_CHANNELS          6

#define SIS8300LLRF_AUX_BYTES                  4

int sis8300llrfdrv_set_aux_param(int down_or_intern, sis8300drv_usr *sisuser, int channel, sis8300llrfdrv_aux_param aux_param, double param_val, double *param_err);
int sis8300llrfdrv_get_aux_param(int down_or_intern, sis8300drv_usr *sisuser, int channel, sis8300llrfdrv_aux_param aux_param, double *param_val);

/* Controller Input selector */
int sis8300llrfdrv_set_ctrl_input_sel(sis8300drv_usr *sisuser, double param_val, double *param_err);
int sis8300llrfdrv_get_ctrl_input_sel(sis8300drv_usr *sisuser, double *param_val);


/* =============== MONITORING INFORMATION  ================ */
int sis8300llrfdrv_get_freq_samp(sis8300drv_usr *sisuser, double *freq_samp) ;

/* =============== INPUT SYNCHRONIZATION  ================ */
#define SIS8300LLRFDRV_INPUT_SYNC_EN_BIT   14
int sis8300llrfdrv_set_inp_sync_en(sis8300drv_usr *sisuser, unsigned enable);

/* =============== SET INSTANCE TYPE ======================*/
int sis8300llrfdrv_set_instance_type(sis8300drv_usr *sisuser, unsigned type);

/* =============== NEW TRIGGER SYSTEM ==================== */
int sis8300llrfdrv_disable_new_trigger(sis8300drv_usr *sisuser);

#ifdef __cplusplus
}
#endif

#endif /* SIS8300LLRFDRV_H_ */
